from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import BernoulliNB
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk import PorterStemmer

import pandas as pd
import math
import utils as u

def get_training_data():
    return pd.read_csv('base_treinamento.csv', delimiter=',')

def get_prediction_data():
    return pd.read_csv('base_previsao.csv', delimiter=',')

def format_text(title, abstract, keywords):
    return "{} {} {}".format(title, abstract, keywords).lower()

def remove_stopwords(text):
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(text)
    words = text.split()
    chosen_words = []
    for w in words:
        if not w in stop_words:
            chosen_words.append(w)
    return ' '.join(chosen_words)

def stemming_words(words, stemmer):
    st_words = []
    for word in words.split():
        new_word = u.remove_all_punctuation(word).strip()
        new_word = stemmer.stem(new_word)
        st_words.append(new_word)
    return ' '.join(st_words)

def prepare_text(title, abstract, keywords, stemmer):
    str_keywords = keywords.strip() if isinstance(keywords, str) else ""
    text = format_text(title, abstract, str_keywords)
    text = remove_stopwords(text)
    text = stemming_words(text, stemmer)
    return text

def data_preparing(dataframe_data, stemmer):
    prepared_data = []
    for idx, row in dataframe_data.iterrows():
        text = prepare_text(row['title'], row["abstract"], row["keywords"], stemmer)
        selected = dataframe_data.at[idx, "select"]
        prepared_data.append([text, selected])
    return prepared_data

def split_data_for_training_validation(data):
    total_amount = len(data)
    training_percentage = 0.75
    training = []
    validation = []
    division = total_amount * training_percentage

    for index in range(0, total_amount):
        if index < division:
            training.append(data[index])
        else:
            validation.append(data[index])

    return training, validation

def pre_processing(stemmer):
    df_data = get_training_data()
    treated_data = data_preparing(df_data, stemmer)
    return split_data_for_training_validation(treated_data)

def execute_training(training_records, vectorizer):
    training_text = [training_record[0] for training_record in training_records]
    training_selected = [training_record[1] for training_record in training_records]

    training_text = vectorizer.fit_transform(training_text)

    return BernoulliNB().fit(training_text, training_selected)

def analyze_text(classifier, vectorizer, text):
    return classifier.predict(vectorizer.transform([text]))

def simple_assessment(validation_records, classifier, vectorizer):
    validation_text = [validation_record[0] for validation_record in validation_records]
    validation_select = [validation_record[1] for validation_record in validation_records]

    total = len(validation_text)
    hits = 0

    for index in range(0, total):
        analysis_result = analyze_text(classifier, vectorizer, validation_text[index])
        hits += 1 if analysis_result[0] == validation_select[index] else 0

    return hits * 100 / total

def complete_assessment(validation_records, classifier, vectorizer):
    validation_text = [validation_record[0] for validation_record in validation_records]
    validation_select = [validation_record[1] for validation_record in validation_records]

    total = len(validation_text)
    truth_positive = 0
    truth_negative = 0
    false_positive = 0
    false_negative = 0

    for index in range(0, total):
        analysis_result = analyze_text(classifier, vectorizer, validation_text[index])
        print("{} {} {}".format(index, analysis_result[0], validation_select[index]))
        truth_positive += 1 if analysis_result[0] == 'T' and validation_select[index] == 'T' else 0
        false_positive += 1 if analysis_result[0] == 'T' and validation_select[index] == 'F' else 0
        truth_negative += 1 if analysis_result[0] == 'F' and validation_select[index] == 'F' else 0
        false_negative += 1 if analysis_result[0] == 'F' and validation_select[index] == 'T' else 0

    return (truth_positive * 100 / total,
            false_positive * 100 / total,
            truth_negative * 100 / total,
            false_negative * 100 / total)

def predict(prediction_dataframe, classifier, vectorizer, stemmer):
    prepared_data = data_preparing(prediction_dataframe, stemmer)
    texts = [prepared_datum[0] for prepared_datum in prepared_data]
    selections = [prepared_datum[1] for prepared_datum in prepared_data]
    total = len(prepared_data)
    print("Predictions: ")
    for index in range(0, total):
        analysis_result = analyze_text(classifier, vectorizer, texts[index])
        print("{} {} {}".format(prediction_dataframe.at[index, 'title'], selections[index], analysis_result[0]))


def main():
    ps = PorterStemmer()
    training_records, validation_records = pre_processing(ps)
    vectorizer = CountVectorizer(binary = 'true')
    classifier = execute_training(training_records, vectorizer)
    df_data = get_training_data()
    text = prepare_text(df_data.at[0, 'title'], df_data.at[0, 'abstract'], df_data.at[0, 'keywords'], ps)
    print(text)
    result = analyze_text(classifier, vectorizer, text)
    print(result)
    total_hits = simple_assessment(validation_records, classifier, vectorizer)
    print("Total hits {} %".format(total_hits))
    total_results = complete_assessment(validation_records, classifier, vectorizer)
    print("Truth positives: {} %".format(total_results[0]))
    print("False positives: {} %".format(total_results[1]))
    print("Truth negatives: {} %".format(total_results[2]))
    print("False negatives: {} %".format(total_results[3]))
    predict(get_prediction_data(), classifier, vectorizer, ps)

if __name__ == "__main__":
    main()