import pandas as pd


def read_csv(name):
    return pd.read_csv(name, keep_default_na=False, delimiter=',', na_values='').to_dict(orient='records')
