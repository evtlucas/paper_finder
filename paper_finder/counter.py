from collections import Counter
import utils as u

def word_count_weight(base, keywords):
    for idx, row in base.iterrows():
        if u.is_nan(row.title_unique):
            continue
        cntr = Counter(row.title_unique.split())
        dict_most_common = dict(cntr.most_common())
        word_count = 0
        for key, weight in keywords.items():
            if key in dict_most_common:
                word_count += dict_most_common[key] * weight
        base.at[idx, 'word_count'] = word_count

def word_counter(text, keywords):
    cntr = Counter(text.split())
    dict_most_common = dict(cntr.most_common())
    word_count = 0
    for key, weight in keywords.items():
        if key in dict_most_common:
            word_count += dict_most_common[key]
    return word_count
