import pandas as pd
import json
import urllib
import time
import keys

def main():
    key = keys.springer_key
    base = pd.read_csv("base.csv")
    abstract_na = list(base["abstract"].isna())
    print("Starting process...")
    for idx, value in enumerate(abstract_na):
        if value and base.at[idx, 'database'] == 'springer':
            doi = base.at[idx, 'doi']
            print(doi)
            time.sleep(2)
            url = "http://api.springernature.com/metadata/json/doi/{}?api_key={}".format(doi, key)
            try:
                response = urllib.request.urlopen(url)
                if response.status == 200:
                    str_json = response.read().decode('utf-8')
                    article_json = json.loads(str_json)
                    base.at[idx, 'abstract'] = article_json['records'][0]['abstract']
            except urllib.error.HTTPError as e:
                base.to_csv("base.csv")
                print(e)

    base.to_csv("base.csv")
    print("Process finished.")

if __name__ == "__main__":
    main()
