from collections import Counter
import paper_io as io

import pandas as pd
import counter as cntr
import glob
import re
import sys
import json
import utils as u
import os

def current_list():
    return pd.read_csv("Extracao.tsv", sep='\t').to_dict(orient='records')

def list_science_direct_to_dict(lines):
    papers = []
    sp_line = ""
    try:
        for line in lines:
            sp_line = line[1]
            paper = dict()
            paper["authors"] = line[0]
            paper["title"] = line[1]
            paper["title"] = paper["title"].lower()
            paper["title_unique"] = u.unique_words(paper["title"])
            paper["doi"] = u.remove_url_from_doi(line[len(line) - 4])
            paper["link"] = line[len(line) - 3]
            paper["abstract"] = line[len(line) - 2]
            paper["keywords"] = u.keywords_handling(line[len(line) - 1])
            paper["database"] = "sciencedirect"
            paper["word_count"] = 0
            papers.append(paper)
    except Exception as e:
        print(str(e))
        print(sp_line)

    return papers

def read_science_direct():
    files = glob.glob('ScienceDirect*.txt')
    lines = []# dict()
    for file in files:
        lines_file = open(file).read()
        lines_file = lines_file[0 : len(lines_file) - 1].split("\n\n")
        for line in lines_file:
            terms = ["Objective", "Objectives", "Result", "Results", "Context", "Method", "Methods",
                    "Conclusion", "Conclusions", "Problem", "Contribution"]
            for term in terms:
                reg_exp = r"[\n]*" + term + r"\n"
                line = re.sub(reg_exp, term, line + ": ")
            lines.append(line.split('\n'))

    return list_science_direct_to_dict(lines)

def transform_source_to_base(set_papers, source_papers, translation, database, title):
    try:
        for paper in source_papers:
            if not paper[title].lower() in set_papers:
                final_paper = {}
                for k, v in translation.items():
                    if len(v) > 0:
                        final_paper[k] = paper[v]
                    else:
                        final_paper[k] = ""
                final_paper["title"] = final_paper["title"].lower()
                final_paper["keywords"] = u.keywords_handling(final_paper["keywords"])
                final_paper["title_unique"] = u.create_title_unique(final_paper["title"], final_paper["keywords"])
                final_paper["database"] = database
                final_paper["word_count"] = 0
                set_papers[paper[title].lower()] = final_paper
    except Exception as e:
        print("Exception at transform_source_to_base: {} \n".format(database))
        print(str(e))
        print(paper)

def transform_ieee_to_base(ieee):
    papers = {}
    titles = [paper["Document Title"].lower() for paper in ieee]
    translation = {'authors' : 'Authors',
                    'title': 'Document Title',
                    'doi': 'DOI',
                    'link': 'PDF Link',
                    'abstract': 'Abstract',
                    'keywords': 'Author Keywords'}
    transform_source_to_base(papers, ieee, translation, "ieee", "Document Title")

    for key, paper in papers.items():
        i = titles.index(paper["title"])
        paper["keywords"] = paper["keywords"] + u.keywords_handling(ieee[i]["IEEE Terms"])
        title_unique = u.create_title_unique(paper["title_unique"], paper["keywords"])
        paper["title_unique"] = u.unique_words(title_unique)
    return papers


def transform_acm_to_base(acm):
    papers = {}
    translation = {'authors' : 'author',
                    'title': 'title',
                    'doi': 'doi',
                    'link': '',
                    'abstract': '',
                    'keywords': 'keywords'}
    transform_source_to_base(papers, acm, translation, "acm", "title")
    return papers

def transform_springer_to_base(springer):
    papers = {}
    translation = {'authors' : 'Authors',
                    'title': 'Item Title',
                    'doi': 'Item DOI',
                    'link': 'URL',
                    'abstract': '',
                    'keywords': ''}
    transform_source_to_base(papers, springer, translation, "springer", "Item Title")
    return papers


def aggregate_bases():
    science_direct = read_science_direct()
    ieee = io.read_csv("ieee.csv")
    acm = io.read_csv("acm.csv")
    springer = io.read_csv("springer.csv")
    set_papers = {}
    papers_science_direct = {}
    for paper in science_direct:
        papers_science_direct[paper["title"].lower()] = paper
    papers_ieee = transform_ieee_to_base(ieee)
    papers_acm = transform_acm_to_base(acm)
    papers_springer = transform_springer_to_base(springer)
    set_papers = {**papers_science_direct, **papers_ieee, **papers_acm, **papers_springer}
    df = pd.DataFrame(set_papers).transpose()
    return df

def main():
    keywords = {"waste": 3, "lean": 2, "software": 2, "development": 2, "agile": 1, "impediments": 2}
    df_base = aggregate_bases()
    cntr.word_count_weight(df_base, keywords)
    sorted_base = df_base.sort_values("word_count", ascending=False)
    if os.path.exists("base.csv"):
        os.remove("base.csv")
    sorted_base.to_csv("base.csv")
    print("The program found {} papers".format(len(df_base.index)))

if __name__ == "__main__":
    main()
