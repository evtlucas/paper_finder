import utils as u
import paper_io as io
import pandas as pd
import math
import counter as cntr

def return_papers():
    return pd.read_csv("base.csv", keep_default_na=False, delimiter=',', na_values='')

def main():
    keywords = {"waste": 3, "lean": 2, "software": 2, "development": 2, "agile": 1, "impediments": 2}
    print("Process started...")
    base = return_papers()
    for idx, row in base.iterrows():
        print("{} {} {}".format(idx, base.at[idx, 'title'], base.at[idx, 'keywords']))
        if u.is_nan(base.at[idx, 'title']):
            continue
        is_nan = u.is_nan(base.at[idx, 'keywords'])
        row_keywords = base.at[idx, 'keywords'] if not is_nan else ""
        base.at[idx, 'keywords'] = u.keywords_handling(row_keywords)
        base.at[idx, "title_unique"] = u.create_title_unique(base.at[idx, "title"], row_keywords)
    cntr.word_count_weight(base, keywords)
    sorted_base = base.sort_values("word_count", ascending=False)
    sorted_base.to_csv("base.csv")
    print("Process finished...")

if __name__ == "__main__":
    main()
