import re
from collections import Counter
import math

def keywords_handling(text):
    return unique_words(remove_punctuation(text).lower())

def remove_punctuation(text):
    list = re.findall(r"[a-zA-Z\-\,\;\.]*", text)
    return " ".join(list)

def remove_all_punctuation(text):
    list = re.findall(r"[a-zA-Z]*", text)
    return " ".join(list)

def remove_url_from_doi(url):
    start = re.search(r"\d+", url).start()
    return url[start : (len(url) - 1)]

def unique_words(text):
    cntr = Counter(text.split(" "))
    dict_words = dict(cntr.most_common())
    unique = []
    for k, v in dict_words.items():
        unique.append(k)
    return " ".join(unique)

def create_title_unique(title, keywords):
    return unique_words(remove_punctuation("{} {}".format(title.lower(), keywords.lower())))

def is_nan(text):
    return isinstance(text, float) and math.isnan(text)
