import pandas as pd
import utils as u
import counter as c
import math

def main():
    print("Starting assessment...")
    dict_keywords = {"waste": 3, "lean": 2, "software": 2, "development": 2, "agile": 1, "impediments": 2}
    base = pd.read_csv("base_selected.csv", index_col=1)
    print(base)
    for idx, row in base.iterrows():
        title = row.title
        abstract = row.abstract if isinstance(row.abstract, str) else ""
        keywords = row.keywords
        base["word_counter"][idx] = 0
        #print("{}, abstract: {}, keywords: {}".format(title, abstract, keywords))
        if 'Not available' not in abstract:
            data_unique = u.title_unique_words(u.remove_punctuation(title + abstract + keywords))
            base["word_counter"][idx] = c.word_counter(data_unique, dict_keywords)
    base.to_csv("base_selected.csv", index=False)
    print("Assessment finished.")

if __name__ == "__main__":
    main()
